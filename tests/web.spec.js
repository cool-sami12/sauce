const { test, expect } = require('@playwright/test');
const {username, password} = require('../data/data.json');

test('Login to sauce demo', async ({page})=>{
  // goto the url 
    await page.goto('https://www.saucedemo.com/')

  //  await page.pause()
  //login 
    await page.locator("#user-name").fill(username)
    await page.locator("#password").fill(password)
    await page.locator("#login-button").click()
    
//    await page.pause()
//  homepage 
    const combobox = page.locator(".product_sort_container")
    const item_1 = await page.locator('[data-test="item-4-title-link"]')
  //asserting that the items are sorted in a-z
    await expect(combobox).toHaveValue("az")
  //  await page.pause()
    await expect(item_1).toHaveText("Sauce Labs Backpack")
    
    //await page.pause()
    // await page.locator(".product_sort_container").click()
   
    // selecting the z-a option
    await page.getByRole('combobox').selectOption('za');
   
    //asserting that the items are sorted in z-a 
    const item_2 = page.locator('[data-test="item-3-title-link"]')
    await expect(combobox).toHaveValue("za")
    //await page.pause()
    await expect(item_2).toHaveText("Test.allTheThings() T-Shirt (Red)")




})