const { test, expect } = require('@playwright/test');

test("Get all entries", async ({request}) =>{
    const response = await request.get('https://api.publicapis.org/entries')
    console.log(await response.json())
    expect(response.status()).toBe(200)

})
test("Get entries by Categories", async ({request})=>{

    const getCategory = await request.get("https://api.publicapis.org/entries",{
        params:{
            "Category":"Authentication & Authorization"
        }
    })
         await expect(getCategory).toBeOK();
        // await expect(getCategory.count()).toBe(7)
        // verifying the number of count for the category above 
        const obj = await getCategory.json()
        console.log(obj.count)
        expect(obj.count).toBe(7)
        // comparing the number of entries for This specified category with the no of count 
        console.log(obj.entries.length)
       expect(obj.entries.length).toEqual(obj.count)

})


